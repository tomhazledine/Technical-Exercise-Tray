import React from "react";

const StepFooter = ({ stepName, handleChangeStep, validity }) => {
    return (
        <div className="step__footer">
            <button
                key={`step-button-${stepName}`}
                type="button"
                className={`button button--${stepName}`}
                onClick={handleChangeStep}
                disabled={validity ? false : true}
            >
                Next step
            </button>
        </div>
    );
};

export default StepFooter;
