import React, { useEffect } from "react";

import { useForm } from "../state/context";

const Done = () => {
    const [form] = useForm();

    useEffect(() => {
        const sanitizedFormResults = Object.keys(form).reduce((acc, key) => {
            return { ...acc, [key]: form[key].value };
        }, {});
        console.log(sanitizedFormResults);
    }, [form]);

    return (
        <div className="step__inner step__message layout__stack">
            <svg
                className="step__message-icon"
                viewBox="0 0 26 25"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M14.46.61c6.673.819 11.42 6.892 10.6 13.566-.82 6.673-6.893 11.418-13.567 10.599-6.673-.82-11.42-6.893-10.6-13.566.82-6.674 6.893-11.42 13.567-10.6zm-.244 1.984C8.64 1.91 3.563 5.876 2.878 11.453c-.684 5.576 3.281 10.652 8.859 11.337 5.577.685 10.653-3.281 11.338-8.858.684-5.578-3.282-10.653-8.859-11.338zM16.81 8.23l1.717 1.193-5.886 8.198-.853-.597-4.323-3.018 1.203-1.719 3.454 2.392L16.81 8.23h-.001z"
                    fillRule="nonzero"
                />
            </svg>
            <h2 className="step__message-heading">Done!</h2>
            <p className="step__message-text">
                Please verify your email address. You should have received an
                email from us already!
            </p>
        </div>
    );
};

export default Done;
