import React, { useEffect, useState } from "react";

import User from "./User";
import Privacy from "./Privacy";
import Done from "./Done";
import StepMarkers from "./StepMarkers";

import { checkStep } from "../utils/validation";

import { useForm } from "../state/context";

const Form = () => {
    const [formValues] = useForm();

    const [steps, setSteps] = useState(() => ({
        current: 0,
        names: ["User", "Privacy", "Done"],
    }));

    const [validity, setValidity] = useState({ 0: false, 1: true });

    const handleChangeStep = () => {
        setSteps((old) => {
            const current =
                old.current <= old.names.length ? old.current + 1 : old.current;
            return {
                ...old,
                current,
            };
        });
    };

    useEffect(() => {
        setValidity((old) => {
            const isValid = checkStep(steps.current, formValues);
            return { ...old, [steps.current]: isValid };
        });
    }, [formValues, steps]);

    return (
        <div className="layout__wrapper">
            <form className="step__wrapper">
                <StepMarkers steps={steps} />

                {steps.names[steps.current] === "User" && (
                    <User
                        handleChangeStep={handleChangeStep}
                        validity={validity[steps.current]}
                    />
                )}
                {steps.names[steps.current] === "Privacy" && (
                    <Privacy
                        handleChangeStep={handleChangeStep}
                        validity={validity[steps.current]}
                    />
                )}
                {steps.names[steps.current] === "Done" && <Done />}
            </form>
        </div>
    );
};

export default Form;
