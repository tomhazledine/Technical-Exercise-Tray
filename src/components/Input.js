import React, { useState } from "react";

import {
    validateRequired,
    validateEmail,
    validatePassword,
} from "../utils/validation";

const Input = ({
    id,
    value,
    label,
    type,
    required = false,
    placeholder = false,
    onChange,
}) => {
    const [valid, setValid] = useState(true);
    const [validationMessage, setValidationMessage] = useState("Invalid");

    const handleInputChange = (value) => {
        let valid = true;
        if (required && !validateRequired(value)) {
            valid = false;
            setValidationMessage("This field is required");
        }
        if (type === "email") {
            const validEmail = validateEmail(value);
            if (!validEmail) {
                valid = false;
                setValidationMessage(
                    "This field must be a valid email address"
                );
            }
        }
        if (type === "password") {
            const validPassword = validatePassword(value);
            if (!validPassword) {
                valid = false;
                setValidationMessage(
                    "Your password must be lomnger than 9 characters and contain at least 1 number, 1 uppercase character, and 1 lowercase character."
                );
            }
        }
        setValid(valid);
        onChange(value, valid);
    };

    return (
        <>
            <label
                className={`input__label ${
                    required ? "input__label--required" : ""
                }`}
                htmlFor={id}
                title={required ? "Required" : undefined}
            >
                {label}
            </label>
            <div>
                <input
                    className={`input ${valid ? "" : "input--invalid"}`}
                    id={id}
                    type={type}
                    value={value ? value : ""}
                    placeholder={placeholder ? placeholder : undefined}
                    onChange={(e) => handleInputChange(e.target.value)}
                    onBlur={() => handleInputChange(value)}
                    autoComplete={label}
                />
                {!valid && (
                    <span className="input__validation">
                        {validationMessage}
                    </span>
                )}
            </div>
        </>
    );
};

export default Input;
