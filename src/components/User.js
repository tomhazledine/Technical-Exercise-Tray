import React from "react";

import Input from "./Input";
import StepFooter from "./StepFooter";

import { useForm } from "../state/context";

const User = ({ handleChangeStep, validity }) => {
    const [form, dispatch] = useForm();
    return (
        <>
            <fieldset className="step__inner layout__stack">
                <div className="form__item layout__stack--small">
                    <Input
                        id="name"
                        value={form["name"].value}
                        label="Name"
                        type="text"
                        placeholder="John Smith"
                        required={true}
                        onChange={(value, validity) =>
                            dispatch({
                                type: "UPDATE_VALUE",
                                id: "name",
                                value,
                                valid: validity,
                            })
                        }
                    />
                </div>
                <div className="form__item">
                    <Input
                        id="role"
                        value={form["role"].value}
                        label="Role"
                        type="text"
                        placeholder="Software Engineer"
                        onChange={(value, validity) =>
                            dispatch({
                                type: "UPDATE_VALUE",
                                id: "role",
                                value,
                                valid: validity,
                            })
                        }
                    />
                </div>
                <div className="form__item">
                    <Input
                        id="email"
                        value={form["email"].value}
                        label="Email"
                        type="email"
                        placeholder="email@example.com"
                        required={true}
                        onChange={(value, validity) =>
                            dispatch({
                                type: "UPDATE_VALUE",
                                id: "email",
                                value,
                                valid: validity,
                            })
                        }
                    />
                </div>
                <div className="form__item">
                    <Input
                        id="password"
                        value={form["password"].value}
                        label="Password"
                        type="password"
                        required={true}
                        onChange={(value, validity) =>
                            dispatch({
                                type: "UPDATE_VALUE",
                                id: "password",
                                value,
                                valid: validity,
                            })
                        }
                    />
                </div>
            </fieldset>
            <StepFooter
                stepName="user"
                handleChangeStep={handleChangeStep}
                validity={validity}
            />
        </>
    );
};

export default User;
