import React from "react";

import Form from "./Form";

import { FormProvider } from "../state/context";

const Main = () => {
    const formValues = {
        name: { value: false, valid: false },
        role: { value: false, valid: false },
        email: { value: false, valid: false },
        password: { value: false, valid: false },
        receive_updates: { value: false, valid: false },
        receive_comms: { value: false, valid: false },
    };

    return (
        <FormProvider options={formValues}>
            <Form />
        </FormProvider>
    );
};

export default Main;
