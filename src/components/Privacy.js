import React from "react";

import Checkbox from "./Checkbox";
import StepFooter from "./StepFooter";

import { useForm } from "../state/context";

const Privacy = ({ handleChangeStep, validity }) => {
    const [form, dispatch] = useForm();

    return (
        <>
            <fieldset className="step__inner layout__stack">
                <div className="form__item layout__stack--small">
                    <Checkbox
                        name="receive_updates"
                        id="receive_updates"
                        label="Receive updates about our product by email."
                        checked={form["receive_updates"].value}
                        onChange={(checked) =>
                            dispatch({
                                type: "UPDATE_VALUE",
                                id: "receive_updates",
                                value: checked,
                                valid: true,
                            })
                        }
                    />
                </div>
                <div className="form__item layout__stack--small">
                    <Checkbox
                        name="receive_comms"
                        id="receive_comms"
                        label="Receive communication by email for other products created by us"
                        checked={form["receive_comms"].value}
                        onChange={(checked) =>
                            dispatch({
                                type: "UPDATE_VALUE",
                                id: "receive_comms",
                                value: checked,
                                valid: true,
                            })
                        }
                    />
                </div>
            </fieldset>
            <StepFooter
                stepName="privacy"
                handleChangeStep={handleChangeStep}
                validity={validity}
            />
        </>
    );
};

export default Privacy;
