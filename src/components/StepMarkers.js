import React from "react";

const StepMarkers = ({ steps }) => {
    const headers = steps.names.map((name, i) => {
        return (
            <li
                key={`step-marker__${name}`}
                className={`step__marker ${
                    i === steps.current ? "step__marker--current" : ""
                }`}
            >
                {name}
            </li>
        );
    });
    return (
        <ul className="step__markers layout__cluster layout__cluster--stretch layout__cluster--no-gutter">
            {headers}
        </ul>
    );
};

export default StepMarkers;
