import React from "react";

const Checkbox = ({ name, id, label, onChange, checked = false }) => {
    const handleCheck = (e) => {
        onChange(e.target.checked, id);
    };

    const handleKeyDown = (e) => {
        if (e.keyCode === 32) {
            onChange(!checked, id);
        }
    };

    return (
        <label className="inline-check check__wrapper" htmlFor={id}>
            <div className="check" tabIndex="0" onKeyDown={handleKeyDown}>
                <input
                    type="checkbox"
                    value={1}
                    name={name}
                    checked={checked}
                    id={id}
                    onChange={handleCheck}
                />
                <label htmlFor={id} className="check__label--sym"></label>
            </div>
            {label && <span className="check__label--visible">{label}</span>}
        </label>
    );
};

export default Checkbox;
