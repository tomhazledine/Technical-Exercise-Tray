export const reducer = (state, action) => {
    switch (action.type) {
        case "UPDATE_VALUE": {
            return {
                ...state,
                [action.id]: { value: action.value, valid: action.valid },
            };
        }
        default: {
            throw new Error(`Unsupported action type: ${action.type}`);
        }
    }
};
