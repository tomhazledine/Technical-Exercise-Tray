import React, { createContext, useContext, useReducer } from "react";

import { reducer } from "./reducer";

const FormContext = createContext();

export const FormProvider = ({ children, options }) => {
    const [formState, dispatchFormState] = useReducer(reducer, options);
    return (
        <FormContext.Provider value={[formState, dispatchFormState]}>
            {children}
        </FormContext.Provider>
    );
};
export const useForm = () => {
    const context = useContext(FormContext);
    if (context === undefined) {
        throw new Error("useFormState must be used within a FormStateProvider");
    }
    return context;
};
