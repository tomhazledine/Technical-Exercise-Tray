export const validateRequired = (value) => {
    if (!value) return false;
    return value.length > 0;
};

export const validateEmail = (email) => {
    if (!email) return false;
    const emailRegex =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(String(email).toLowerCase());
};

export const validatePassword = (password) => {
    if (!password) return false;

    const chars = password.split("");

    if (chars.length <= 9) return false;

    const numbers = chars.filter((char) => char.match(/^-?\d+$/));
    if (numbers.length <= 0) return false;

    const letters = chars.filter((char) => !char.match(/^-?\d+$/));
    const uppercaseChars = letters.filter(
        (char) => char.toUpperCase() === char
    );
    if (uppercaseChars.length <= 0) return false;

    const lowercaseChars = letters.filter(
        (char) => char.toLowerCase() === char
    );
    if (lowercaseChars.length <= 0) return false;

    return true;
};

export const checkStep = (step, values) => {
    switch (step) {
        case 0:
            if (
                values.name.valid &&
                values.email.valid &&
                values.password.valid
            ) {
                return true;
            }
            return false;
        case 1:
            return true;
        default:
            return false;
    }
};
