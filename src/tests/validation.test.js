import {
    validateEmail,
    validatePassword,
    validateRequired,
} from "../utils/validation";

describe("Validation utils", () => {
    it("validates required", () => {
        expect(validateRequired("")).toBe(false);
        expect(validateRequired("abc")).toBe(true);
        expect(validateRequired("abc d")).toBe(true);
    });

    it("validates email", () => {
        expect(validateEmail("")).toBe(false);
        expect(validateEmail("abc")).toBe(false);
        expect(validateEmail("abc@d")).toBe(false);
        expect(validateEmail("abcd@d.eo")).toBe(true);
    });

    it("validates password]", () => {
        expect(validatePassword("")).toBe(false);
        expect(validatePassword("abc")).toBe(false);
        expect(validatePassword("abcdefghij")).toBe(false);
        expect(validatePassword("abcdefghijABC")).toBe(false);
        expect(validatePassword("abcdefghij123")).toBe(false);
        expect(validatePassword("123ABCDEDGF")).toBe(false);
        expect(validatePassword("abcdefghijABC123")).toBe(true);
    });
});
