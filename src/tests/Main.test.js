import { render, screen } from "@testing-library/react";
import Main from "../components/Main";

describe("Main component", () => {
    it("renders without errors (smoke test)", () => {
        render(<Main />);
        const userText = screen.getByText(/user/i);
        expect(userText).toBeInTheDocument();
    });
});
